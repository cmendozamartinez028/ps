import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import  sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine


sqlite_file_name = '../database.sqlite'
base_dir = os.path.abspath(os.path.realpath(__file__))
database_url = 'sqlite:///' + os.path.join(base_dir, sqlite_file_name)



engine = create_engine(database_url, echo=True)
Session = sessionmaker(bind=engine)

Base = declarative_base()

def get_db():
    db = Session()
    try:
        yield db
    finally:
        db.close()