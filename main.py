# importaciones del sistema
import uvicorn
from fastapi import FastAPI

# Importanto los middlewares
from fastapi.middleware.cors import CORSMiddleware


# Importando la base y la configuracion
from config.base import Base
from config.config import settings

# importaciones de db 
from config.db import engine


# ------------------------> Importaciones de los routers <------------------------

from router.usuarioAuth import userAuth_router
from router.usuarioCrud import usuarioCrud_router
from router.eventos import eventos_router
from router.documentos import documentos_router
from router.documentos_alistamiento import doc_alistamiento_router
from router.documentos_ejecucion import doc_ejecucion_router
from router.documentos_evaluacion import doc_evaluacion

# origins = [
#     settings.CLIENT_ORIGIN,
# ]

origins = [
    "http://localhost:3000",
    "http://localhost:5000",
    "http://localhost:8000",
    "http://147.182.254.5:5000"
    "http://147.182.254.5:8000"
    ]


# Inicio de la aplicacion

app = FastAPI()

app.title = "API GHIBLI"
app.version = "1.0.0"


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(userAuth_router, prefix="/auth", tags=["LOGIN AND REGISTER"])
app.include_router(usuarioCrud_router, prefix="/users", tags=["USERS"])
app.include_router(eventos_router, prefix="/eventos", tags=["EVENTOS"])
app.include_router(documentos_router,prefix="/documentos", tags=["DOCUMENTOS"])
app.include_router(doc_alistamiento_router, prefix="/doc/alistamiento",tags=["DOC_AListamiento"])
app.include_router(doc_ejecucion_router, prefix="/doc/ejecucion",tags=["DOC_EJECUCION"])
app.include_router(doc_evaluacion,prefix="/doc/evaluacion",tags=["DOC_EVALUACION"])

# Conexion a la base de datos

Base.metadata.create_all(bind=engine)

@app.get('/', tags=["Inicio"])
@app.get("/")
def read_root():
    return {"Welcome to the API of Studio Ghibli"}


if __name__ == "__main__":
    uvicorn.run("main:app", port=5000, reload=True)

