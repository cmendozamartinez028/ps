# Configuracion desde la base de datos
from config.db import Base

# Configuraciones de SQLALCHEMY

from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP, text,Table
from sqlalchemy.types import Boolean
from sqlalchemy.orm import relationship, declarative_base
from sqlalchemy import ForeignKey


# Agregando bases

Base_orm = declarative_base();

events_person = Table('events_person', Base.metadata,
    Column('event_id', ForeignKey('eventos.id'), primary_key=True),
    Column('person_id', ForeignKey('usuario.id'), primary_key=True)
)



class Usuario(Base):
    __tablename__ = 'usuario'
    id = Column(Integer, primary_key=True)
    name = Column(String(50),  nullable=False)
    email = Column(String(50), unique=True, nullable=False)
    password = Column(String(50), nullable=False)
    cedula = Column(Integer, unique=True, nullable=False)
    verified = Column(Boolean, nullable=False, default=False)
    role = Column(String(50), default='user', nullable=False)
    departamento = Column(String(50), nullable=False)
    municipio = Column(String(50), nullable=False)
    created_at = Column(TIMESTAMP(timezone=True),
                        nullable=False, default=text('now()'))
    updated_at = Column(TIMESTAMP(timezone=True),
                        nullable=False, default=text('now()'))

    
    eventos = relationship('Eventos',secondary="events_person", backref="usuario")

    def __ini__(self, name, email, password, cedula, verified, role, departamento, municipio, created_at, updated_at):
        self.name = name
        self.email = email
        self.password = password
        self.cedula = cedula
        self.verified = verified
        self.role = role
        self.departamento = departamento
        self.municipio = municipio
        self.created_at = created_at
        self.updated_at = updated_at


class Eventos(Base):
    __tablename__ = 'eventos'

    # id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    id = Column(Integer, primary_key=True)
    # user_id = Column(Integer(), ForeignKey('usuario.id'))
    name = Column(String, index=True)
    inicio = Column(TIMESTAMP(timezone=True),
                        nullable=False, default=text('now()'))
    fin = Column(TIMESTAMP(timezone=True),
                        nullable=False, default=text('now()'))
    created_at = Column(TIMESTAMP(timezone=True),
                        nullable=False, default=text('now()'))
    

    author = relationship('Usuario', secondary="events_person",backref ="events_person")

    def __ini__(self,name, user_id,inicio,fin,created_at):
        self.name = name
        self.inicio = inicio
        self.fin = fin
        self.created_at = created_at
        
        
    


