# Importaciones de FastApi
from fastapi import APIRouter, UploadFile,File,Form
from fastapi.responses import FileResponse, JSONResponse

# Importaciones del sistema

from os import getcwd, remove
from shutil import rmtree
import os


documentos_router = APIRouter()

# Endpoint para subir un archivo en fastapi 

@documentos_router.post("/upload")
async def upload_file(file:UploadFile=File(...)):
    with open(getcwd() + "/" + "documentos/" + file.filename, "wb") as myfile:
        content = await file.read()
        myfile.write(content)
        myfile.close()
    return "success"


# Endpoint para obtener un archivo en fastApi

@documentos_router.get("/file/{name_file}")
def get_file(name_file:str):
    # Nota: Agregar la extension ya que es necesaria para que guarde el archivo 
    return FileResponse(getcwd()+ "/" + "documentos/" + name_file)


# Endpoint para descargar los archivos 

@documentos_router.get("/download/{name_file}")
def download_file(name_file:str):
    return FileResponse(getcwd()+ "/" + "documentos/" + name_file, media_type="application/octet-stream",filename=name_file)


# Endpoint para eliminar los archivos 

@documentos_router.delete("/delete/{name_file}")
def delete_file(name_file: str):
    try:
        remove(getcwd()+ "/" + "documentos/" + name_file)
        return JSONResponse(content={
            "removed": True,
            "message": "File  found"},
            status_code=404
        )
    except FileNotFoundError:
        return JSONResponse(content={
            "removed": False,
            "message": "File not found"},
            status_code=404
        )
    
@documentos_router.delete("/folder")
def delete_file(folder_name: str = Form(...)):
    rmtree(getcwd() + "/" +folder_name)
    return JSONResponse(content={
        "removed": True
    }, status_code=200)


@documentos_router.post("/folder")
def add_folder(folder_name: str = Form(...)):
    os.mkdir(getcwd() + "/" + folder_name)
    return JSONResponse(content={
        "add": True
    }, status_code=200)



