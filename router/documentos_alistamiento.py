# Importaciones de FastApi
from fastapi import APIRouter, UploadFile,File,Form
from fastapi.responses import FileResponse, JSONResponse

# Importaciones del sistema

from os import getcwd, remove
from shutil import rmtree
import os



doc_alistamiento_router = APIRouter()

# [Formulario individual]

# Get

@doc_alistamiento_router.get("/file/{name_file}")
def get_file(id:int,name_file:str):
    name_file = name_file + ".pdf" # Agregar pdf
    id = str(id)
    carpeta = "N" + id + "/"
    return FileResponse(getcwd()+ "/" + "documentos/" + "Alistamiento/" + carpeta + name_file)




@doc_alistamiento_router.get("/file/sub/{name_file}")
def get_file_sub(id:int,id_sub:float,name_file:str):
    id = str(id)
    id_sub = str(id_sub)
    principal = "N" + id + "/"
    secundaria = id_sub + "/"
    name_file = name_file + ".pdf"
    return FileResponse(getcwd()+ "/" + "documentos/" + "Alistamiento/" + principal + secundaria + name_file)




@doc_alistamiento_router.get("/download/sub/{name_file}")
def download_file_sub(id:int,id_sub:float,name_file:str):
    id = str(id)
    id_sub = str(id_sub)
    principal = "N" + id + "/"
    secundaria = id_sub + "/"
    name_file = name_file + ".pdf"
    return FileResponse(getcwd()+ "/" + "documentos/" + "Alistamiento/" + principal + secundaria  + name_file, media_type="application/octet-stream",filename=name_file)



@doc_alistamiento_router.get("/download/{name_file}")
def download_file(id:int,name_file:str):
    id = str(id)
    carpeta = "N" + id + "/"
    name_file = name_file + ".pdf"
    return FileResponse(getcwd()+ "/" + "documentos/" + "Alistamiento/" + carpeta  + name_file, media_type="application/octet-stream",filename=name_file)



# Endpoint para agregar elemetos de cualquier folder del formulario

@doc_alistamiento_router.post("/upload")
async def upload_file(id:int,file:UploadFile=File(...)):
    id = str(id)
    file.filename = id + ".pdf" # ---> Cambiar
    carpeta = "N" + id + "/"
    with open(getcwd() + "/" + "documentos/" + "Alistamiento/" + carpeta +  file.filename, "wb") as myfile:
        content = await file.read()
        myfile.write(content)
        myfile.close()
    return "success"


@doc_alistamiento_router.post("/upload/sub")
async def upload_sub(id: int,id_sub:float,file:UploadFile=File(...)):
    id = str(id)
    id_sub = str(id_sub)
    file.filename = id + ".pdf" # ---> Cambiar
    principal = "N" + id + "/"
    secundaria = id_sub + "/"

    with open(getcwd() + "/" + "documentos/" + "Alistamiento/" + principal + secundaria + file.filename, "wb") as myfile:
        content = await file.read()
        myfile.write(content)
        myfile.close()
    return "success"


# Endpoint para eliminar elementos de cualquier folder del formulario

@doc_alistamiento_router.delete("/delete/{name_file}")
def delete_file(id:int,name_file:str):
    id = str(id)
    carpeta = "N" + id + "/"
    name_file = name_file + ".pdf"
    try:
        remove(getcwd()+ "/" + "documentos/" + "Alistamiento/" + carpeta + name_file)
        return JSONResponse(content={
            "removed": True,
            "message": "File  found"},
            status_code=404
        )
    except FileNotFoundError:
        return JSONResponse(content={
            "removed": False,
            "message": "File not found"},
            status_code=404
        )


@doc_alistamiento_router.delete("/delete/sub/{name_file}")
def delete_file_sub(id:int,id_sub:float,name_file:str):
    id = str(id)
    id_sub = str(id_sub)
    principal = "N" + id + "/"
    secundaria = id_sub + "/"
    name_file = name_file + ".pdf"
    try:
        remove(getcwd()+ "/" + "documentos/" + "Alistamiento/" + principal + secundaria + name_file)
        return JSONResponse(content={
            "removed": True,
            "message": "File  found"},
            status_code=404
        )
    except FileNotFoundError:
        return JSONResponse(content={
            "removed": False,
            "message": "File not found"},
            status_code=404
        )


