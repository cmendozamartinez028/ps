# Importaciones de FastApi
from fastapi import APIRouter, UploadFile,File,Form
from fastapi.responses import FileResponse, JSONResponse

# Importaciones del sistema

from os import getcwd, remove
from shutil import rmtree
import os




doc_ejecucion_router = APIRouter()


@doc_ejecucion_router.post("/upload")
async def upload_file(id:int,file:UploadFile=File(...)):
    id = str(id)
    file.filename = id + ".pdf" # ---> Cambiar
    carpeta = "N" + id + "/"
    with open(getcwd() + "/" + "documentos/" + "Ejecucion/" + carpeta +  file.filename, "wb") as myfile:
        content = await file.read()
        myfile.write(content)
        myfile.close()
    return "success"



@doc_ejecucion_router.get("/download/{name_file}")
def download_file(id:int,name_file:str):
    id = str(id)
    carpeta = "N" + id + "/"
    name_file = name_file + ".pdf"
    return FileResponse(getcwd()+ "/" + "documentos/" + "Ejecucion/" + carpeta  + name_file, media_type="application/octet-stream",filename=name_file)



@doc_ejecucion_router.get("/file/{name_file}")
def get_file(id:int,name_file:str):
    id = str(id)
    carpeta = "N" + id + "/"
    name_file = name_file + ".pdf"
    return FileResponse(getcwd()+ "/" + "documentos/" + "Ejecucion/" + carpeta + name_file)


@doc_ejecucion_router.delete("/delete/{name_file}")
def delete_file(id:int,name_file:str):
    id = str(id)
    carpeta = "N" + id + "/"
    name_file = name_file + ".pdf"
    try:
        remove(getcwd()+ "/" + "documentos/" + "Ejecucion/" + carpeta + name_file)
        return JSONResponse(content={
            "removed": True,
            "message": "File  found"},
            status_code=404
        )
    except FileNotFoundError:
        return JSONResponse(content={
            "removed": False,
            "message": "File not found"},
            status_code=404
        )
    
