# Importaciones de fastApi
from fastapi import APIRouter, Depends, HTTPException, status, Response


# Importaciones del sistema 
from typing import List



# importaciones de los modelos
from models.usuario import Eventos
from models.usuario import Usuario
from models.usuario import events_person


# Importaciones de la base de datos
from config.db import get_db
from config.db import Session

# Configuraciones de Autentificacion

from config.auth import require_user

# Importar un esquema 
from schemas import eventos_sch

# imporntado load

from sqlalchemy.orm import joinedload


# Importaciones de pruebas 

from sqlalchemy.future import select
from sqlalchemy import delete







eventos_router = APIRouter()

@eventos_router.post("/eventos/{id}")
def save_event(info:eventos_sch.EventosSchema,db: Session = Depends(get_db)):
    b = info.dict()

    auths = b['author']
    ids = [a['id'] for a in auths]
    authors = []
    for id in ids:
        a = db.execute(select(Usuario).where(Usuario.id == id))
        a= a.scalars().first()
        if a is None:
            return {"Mensaje":"Usuario no encontrado"}
            return Response(status_code=status.HTTP_200_OK)
        authors.append(a)
    # id=b['id'],
    event = Eventos(name=b['name'],inicio=b['inicio'],fin=b['fin'],created_at=b['created_at'])
    event.author = authors

    print("Valor: ", event)
    db.add(event)
    db.commit()
    db.refresh(event)
    db.close()
    return event



@eventos_router.delete("/eventos/{id}")
def delete_event(id:int,db: Session = Depends(get_db)):
    query = delete(events_person).where(events_person.c.event_id == id)
    db.execute(query)
    query = delete(Eventos).where(Eventos.id==id)
    db.execute(query)
    db.commit()
    db.close()
    return {"Mensaje":"Eliminado con exito"}
    


@eventos_router.get("/eventos/{id}", response_model=eventos_sch.UsuariosSchema)
def get_events(id:int,db: Session = Depends(get_db)):
    session = Session()
    user = db.query(Usuario).filter(Usuario.id==id).first()
    if not user:
         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Id not found")

    db_author = db.query(Usuario).options(joinedload(Usuario.eventos)).where(Usuario.id==id).one()
    
    session.close()
    db.close()
    return db_author
    


@eventos_router.put("/eventos/{id}")
def update_event(id:int,payload:eventos_sch.EventoUpdate,db: Session = Depends(get_db)):
    session = Session()
    # Aqui va la validacion del evento
    if not db.query(Eventos).filter(Eventos.id == id).first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Event not found")
    
    session.query(Eventos).filter(Eventos.id == id).update(payload.dict())
    session.commit()
    session.close()

    return {"Mensaje": "Evento actualizado con exito"}

