# importaciones del sistema
from datetime import timedelta
from config.config import settings
from utils import utils  # importando utilidades

# importaciones de FastAPI Y pydantic
from fastapi import APIRouter, Depends, HTTPException, status, Response, Request
from schemas import usuario_sch
from pydantic import EmailStr

# importaciones de seguridad

from config.auth import AuthJWT
from config.auth import require_user

# configuracion de base de datos
from config.db import get_db
from config.db import Session

# importaciones del modelo 
from models import usuario



userAuth_router = APIRouter()
ACCESS_TOKEN_EXPIRES_IN  = settings.ACCESS_TOKEN_EXPIRES_IN
REFRESH_TOKEN_EXPIRES_IN  = settings.REFRESH_TOKEN_EXPIRES_IN

@userAuth_router.post('/register/{email}', status_code=status.HTTP_201_CREATED, response_model=usuario_sch.UserResponseSchema)
async def register(payload: usuario_sch.CreateUserSchema, db: Session = Depends(get_db)):
    user = db.query(usuario.Usuario).filter(usuario.Usuario.email== payload.email).first()
    userCedula = db.query(usuario.Usuario).filter(usuario.Usuario.cedula== payload.cedula).first()
    if user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Email already registered")
    
    if userCedula:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Cedula already registered")

    if(payload.password != payload.passwordConfirm):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Passwords do not match")

    payload.password = utils.hash_password(payload.password)
    del payload.passwordConfirm
    payload.role = 'user'
    payload.verified = True
    payload.email = EmailStr(payload.email.lower())
    new_user = usuario.Usuario(**payload.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


@userAuth_router.post('/login')
def login(payload: usuario_sch.LoginUserSchema, response: Response, db: Session = Depends(get_db), Authorize: AuthJWT = Depends()):
    user = db.query(usuario.Usuario).filter(
        usuario.Usuario.email == EmailStr(payload.email.lower())).first()
        
    if not user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Incorrect Email or Password')
    
    if not user.verified:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail='Please verify your email address')

    if not utils.verify_password(payload.password, user.password):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Incorrect Email or Password')
    
   
    access_token = Authorize.create_access_token(
        subject=str(user.id), expires_time=timedelta(minutes=ACCESS_TOKEN_EXPIRES_IN))
    refresh_token = Authorize.create_refresh_token(
        subject=str(user.id), expires_time=timedelta(minutes=REFRESH_TOKEN_EXPIRES_IN))
        
    response.set_cookie('access_token', access_token, ACCESS_TOKEN_EXPIRES_IN * 60, ACCESS_TOKEN_EXPIRES_IN * 60, '/', None, False, True, 'lax')
    
    response.set_cookie('refresh_token', refresh_token, REFRESH_TOKEN_EXPIRES_IN * 60, REFRESH_TOKEN_EXPIRES_IN * 60, '/', None, False, True, 'lax')
    
    response.set_cookie('logged_in', 'True', ACCESS_TOKEN_EXPIRES_IN * 60, ACCESS_TOKEN_EXPIRES_IN * 60, '/', None, False, False, 'lax')
                            
    return {'status': 'success', 'access_token': access_token}


@userAuth_router.get('/refresh')
def refresh_token(response: Response, request: Request, Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    try:
        print(Authorize._refresh_cookie_key)
        Authorize.jwt_refresh_token_required()

        user_id = Authorize.get_jwt_subject()

        if not user_id:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail='Could not refresh access token')

        user = db.query(usuario.Usuario).filter(usuario.Usuario.id == user_id).first()

        if not user:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail='The user belonging to this token no logger exist')

        access_token = Authorize.create_access_token(subject=str(user.id), expires_time=timedelta(minutes=ACCESS_TOKEN_EXPIRES_IN))

    except Exception as e:
        error = e.__class__.__name__
        if(error== 'MissingTok'):
            raise HTTPException(
                 status_code=status.HTTP_400_BAD_REQUEST, detail='Please provide refresh token')
        
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=error)

    response.set_cookie('access_token', access_token, ACCESS_TOKEN_EXPIRES_IN * 60,
                        ACCESS_TOKEN_EXPIRES_IN * 60, '/', None, False, True, 'lax')
    response.set_cookie('logged_in', 'True', ACCESS_TOKEN_EXPIRES_IN * 60,
                        ACCESS_TOKEN_EXPIRES_IN * 60, '/', None, False, False, 'lax')
   
    return {'access_token': access_token}



@userAuth_router.get('/logout', status_code=status.HTTP_200_OK)
def logout(response: Response, Authorize: AuthJWT = Depends(), user_id: str = Depends(require_user)):
    Authorize.unset_jwt_cookies()
    response.set_cookie('logged_in', '', -1)

    return {'status': 'success'}
        

@userAuth_router.put('/users/change-password/{id}')
async def update_user_password(id: int, payload:usuario_sch.UpdateUserSchemaPassword ,db: Session = Depends(get_db)):
    session = Session()
    user = db.query(usuario.Usuario).filter(usuario.Usuario.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    
    if(payload.password != payload.passwordConfirm):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Las contraseñas no coinciden")
    
    payload.password = utils.hash_password(payload.password)
    del payload.passwordConfirm
    update_data = payload.dict()
    session.query(usuario.Usuario).filter(usuario.Usuario.id == id).update(update_data)
    session.commit()
    session.close()
    return {"Mensaje": "Usuario actualizado con exito"}


@userAuth_router.get('/session/user/')
def obtain_session_user( Authorize: AuthJWT = Depends(), db: Session = Depends(get_db)):
    Authorize.jwt_required()
    user_id = Authorize.get_jwt_subject()
    user = db.query(usuario.Usuario).filter(usuario.Usuario.id == user_id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return user
    