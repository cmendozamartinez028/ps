# importaciones de fastapi
from fastapi import APIRouter, Depends, HTTPException, status, Response, Request


# importaciones de base de datos
from config.db import get_db
from config.db import Session

# importaciones de seguridad
from config.auth import require_user

# importando servicio
from services.usuarioServ import UsuarioService

# importando esquemas
from schemas import usuario_sch

# importando modelo
from models import usuario
from models.usuario import events_person

# importando Utilidades
from utils import utils

# Importaciones de la base de datos

from sqlalchemy import delete


usuarioCrud_router = APIRouter()


@usuarioCrud_router.get('/users')
def get_users(db: Session = Depends(get_db),user_id: str = Depends(require_user)):
    session = Session()
    usuario = UsuarioService(session).get_all()
    session.close()
    return usuario


@usuarioCrud_router.get('/users/{id}')
def get_user(id: int, db: Session = Depends(get_db),user_id: str = Depends(require_user)):
    session = Session()
    usuario = UsuarioService(session).get_by_id(id)
    session.close()
    if usuario is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    return usuario

@usuarioCrud_router.delete('/users/{id}')
def get_user(id: int, db: Session = Depends(get_db),user_id: str = Depends(require_user)):
    session = Session()

    if not db.query(usuario.Usuario).filter(usuario.Usuario.id == id).first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    
    # Elimininando la relacion con los eventos
    session.query(events_person).where(events_person.c.person_id == id).delete()
    

    session.query(usuario.Usuario).filter(usuario.Usuario.id == id).delete()
    session.commit()
    session.close()
    return {"Mensaje": "Usuario eliminado con exito"}



@usuarioCrud_router.put('/users/{id}')
def update_user(id: int, payload: usuario_sch.UpdateUserSchemaGeneral, db: Session = Depends(get_db),user_id: str = Depends(require_user)):
    session = Session()
    if not db.query(usuario.Usuario).filter(usuario.Usuario.id == id).first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    

    # -> Nota importante
    # -> Se necesita la validacion para que no se actualice con el schema predeterminas []
    
    session.query(usuario.Usuario).filter(usuario.Usuario.id == id).update(payload.dict())
    session.commit()
    session.close()
    return {"Mensaje": "Usuario actualizado con exito"}

# -> Probar
@usuarioCrud_router.put('/users/date/{id}')
def update_user_date(id: int, payload: usuario_sch.UpdateUserSchema, db: Session = Depends(get_db),user_id: str = Depends(require_user)):
    session = Session()
    if not db.query(usuario.Usuario).filter(usuario.Usuario.id == id).first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    
    session.query(usuario.Usuario).filter(usuario.Usuario.id == id).update(payload.dict())
    session.commit()
    session.close()
    return {"Mensaje": "Usuario actualizado con exito"}


@usuarioCrud_router.put('/users/change-password/{id}')
async def update_user_password(id: int, payload:usuario_sch.UpdateUserSchemaPassword ,db: Session = Depends(get_db),user_id: str = Depends(require_user)):
    session = Session()
    user = db.query(usuario.Usuario).filter(usuario.Usuario.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    
    if(payload.password != payload.passwordConfirm):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Las contraseñas no coinciden")
    
    payload.password = utils.hash_password(payload.password)
    del payload.passwordConfirm
    update_data = payload.dict()
    session.query(usuario.Usuario).filter(usuario.Usuario.id == id).update(update_data)
    session.commit()
    session.close()
    return {"Mensaje": "Usuario actualizado con exito"}


