from pydantic import BaseModel
from typing import List

from datetime import datetime

class Usuario(BaseModel):
    name:str
    eventos:str

    class Config:
        orm_mode = True 



class UsuarioBase(BaseModel):
    id: int 
    # name: str

    class Config:
        orm_mode = True

class EventoBaseCreate(BaseModel):
    name : str
    inicio : datetime
    fin : datetime
    created_at: datetime

    class Config:
        orm_mode = True


class EventoBase(BaseModel):
    id : int
    name : str
    inicio : datetime
    fin : datetime
    created_at: datetime

    class Config:
        orm_mode = True


class EventoUpdate(BaseModel):
    name : str
    inicio : datetime
    fin : datetime
    created_at: datetime



class UsuariosSchema(UsuarioBase):
    eventos : List[EventoBase]

class EventosSchema(EventoBaseCreate):
    author : List[UsuarioBase]
    
    





























# Funcional


# class EventosBaseSchema(BaseModel):
#     name : str
#     inicio: datetime
#     fin: datetime
#     created_at: datetime


    


# class EventosCreate(EventosBaseSchema):
#     pass


# class Evento(EventosBaseSchema):
#     id: int 
#     user_id : int 

#     class Config:
#         orm_mode = True


