from pydantic import BaseModel, EmailStr, constr

# importaciones del fecha y tiempo

from datetime import datetime

class UserBaseSchema(BaseModel):
    name : str
    email : EmailStr

    class Config:
        orm_mode = True


class CreateUserSchema(UserBaseSchema):
    password : constr(min_length=8, max_length=16) # Se puede cambiar el maximo y minimo
    passwordConfirm : str
    cedula : int
    role : str = 'user'
    departamento : str = 'Bolivar'
    municipio : str = 'Cartagena'
    verified : bool = False
    created_at: datetime
    updated_at: datetime

class UserResponseSchema(UserBaseSchema):
    id : int
    cedula : int
    role : str
    departamento : str
    municipio : str
    created_at: datetime
    updated_at: datetime


class LoginUserSchema(BaseModel):
    email : str = 'ca@utb.com'
    password : constr(min_length=8)

class UpdateUserSchema(BaseModel):
    updated_at: datetime

class UpdateUserSchemaGeneral(BaseModel):
    name : str
    email : EmailStr
    cedula : int
    role : str
    departamento : str
    municipio : str


class UpdateUserSchemaPassword(BaseModel):
    password : constr(min_length=8, max_length=16) # Se puede cambiar el maximo y minimo
    passwordConfirm : str
    


# email : EmailStr
#     password : constr(min_length=8)
