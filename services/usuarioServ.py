from models.usuario import Usuario as UsuarioModel
from sqlalchemy.orm import load_only


class UsuarioService():
    def __init__(self, db):
        self.db = db

    def get_all(self):
        usuario = self.db.query(UsuarioModel).options(load_only(
            UsuarioModel.id, 
            UsuarioModel.name,      
            UsuarioModel.email,
            UsuarioModel.cedula,
            UsuarioModel.verified,
            UsuarioModel.role,
            UsuarioModel.departamento,
            UsuarioModel.municipio,
            UsuarioModel.created_at,
            UsuarioModel.updated_at
            

            )).all()
        return usuario
    
    def get_by_id(self, id):
        usuario = self.db.query(UsuarioModel).options(load_only(
            UsuarioModel.id, 
            UsuarioModel.name,      
            UsuarioModel.email,
            UsuarioModel.cedula,
            UsuarioModel.verified,
            UsuarioModel.role,
            UsuarioModel.departamento,
            UsuarioModel.municipio,
            UsuarioModel.created_at,
            UsuarioModel.updated_at

            )).filter(UsuarioModel.id == id).first()
        return usuario
        